<?php

namespace Gabelbart\Laravel\Nova\ToolbarTools;

use Illuminate\Http\Request;

class ToolbarToolsService
{
    /**
     * All of the registered Nova tools.
     *
     * @var array
     */
    public array $toolbarTools = [];

    /**
     * Register new tools with Nova.
     *
     * @param  array  $tools
     * @return static
     */
    public function toolbarTools(array $tools): ToolbarToolsService
    {
        $this->toolbarTools = array_merge(
            $this->toolbarTools,
            $tools
        );

        return $this;
    }

    /**
     * Get the tools registered with Nova.
     *
     * @return array
     */
    public function registeredToolbarTools(): array
    {
        return $this->toolbarTools;
    }

    /**
     * Boot the available Nova tools.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function bootToolbarTools(Request $request)
    {
        collect($this->availableToolbarTools($request))->each->boot();
    }

    /**
     * Get the tools registered with Nova.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function availableToolbarTools(Request $request): array
    {
        return collect($this->toolbarTools)->filter->authorize($request)->all();
    }
}
