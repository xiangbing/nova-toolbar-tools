<?php

namespace Gabelbart\Laravel\Nova\ToolbarTools\Traits;

use Illuminate\Database\Eloquent\Model;

use Gabelbart\Laravel\Nova\ToolbarTools\Tools\ToolbarResource;

trait HasToolbarSelection
{
    public static function getToolbarSelection(): ?Model
    {
        return ToolbarResource::sessionValueFor(static::class);
    }
    public static function getToolbarSelectionOrFail(): Model
    {
        return (static::$model)::findorFail(ToolbarResource::rawSessionValueFor(static::class));
    }
}
