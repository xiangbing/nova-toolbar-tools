<?php

namespace Gabelbart\Laravel\Nova\ToolbarTools\Tools;

use Illuminate\Http\Request;

use Laravel\Nova\AuthorizedToSee;
use Laravel\Nova\Makeable;
use Laravel\Nova\Metable;
use Laravel\Nova\ProxiesCanSeeToGate;

abstract class ToolbarTool implements \JsonSerializable
{
    use AuthorizedToSee,
        Makeable,
        Metable,
        ProxiesCanSeeToGate;

    /**
     * The tool's component.
     *
     * @var string
     */
    public string $component = 'toolbar-tool';

    /**
     * Determine if the element should be displayed for the given request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    public function authorize(Request $request): bool
    {
        return $this->authorizedToSee($request);
    }

    /**
     * Perform any tasks that need to happen on tool registration.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Build the view that renders the navigation links for the tool.
     *
     * @return string
     */
    public function component(): string
    {
        return $this->component;
    }

    public function jsonSerialize()
    {
        return array_merge([
            'component' => $this->component(),
        ], $this->meta());
    }

}
