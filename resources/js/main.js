Nova.booting((Vue, router, store) => {
    Vue.component(
        'toolbar-tool-list',
        require('./components/ToolbarToolList.vue')
    )
    Vue.component(
        'toolbar-resource',
        require('./components/ToolbarResource.vue')
    )
})
